﻿using Microsoft.Xaml.Behaviors;
using MqttCleint.Model;
using System;
using System.Windows;


namespace MqttCleint
{
    public class CloseWindowBehavior : Behavior<Window>
    {
        protected override void OnAttached()
        {
            Window window = this.AssociatedObject;
            if (window != null) window.Closing += Window_Closing;
        }

        private void Window_Closing(object sender, EventArgs e)
        {
            MqttConnHandler.Client.Disconnect();
        }
    }
}

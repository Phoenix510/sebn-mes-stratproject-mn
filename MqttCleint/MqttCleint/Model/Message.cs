﻿using System;

namespace MqttCleint.Model
{
    public class Message
    {
        public Guid Id { get; set; }

        private string _nick;
        public string Nick { get { return _nick ?? "Anonymous"; } set { _nick = value; } }
        public string Content { get; set; }
        public DateTime DateOfCreation { get; set; }

        public DateTime LocalDateOfCreation => (DateTime.SpecifyKind(DateOfCreation, DateTimeKind.Utc)).ToLocalTime();
    }
}

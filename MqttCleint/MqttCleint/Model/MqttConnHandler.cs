﻿using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace MqttCleint.Model
{
    public static class MqttConnHandler
    {
        public static MqttClient Client;
        private static string _clientId;
        public static ObservableCollection<Message> Messages = new ObservableCollection<Message>();

        public static async Task<bool> SubscribeAndConnect(string topic, string address) 
        {
            //return await Task.Run<bool>(() => {
            try
            {
                IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
                Client = new MqttClient(ipAddress);

                Client.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
                _clientId = Guid.NewGuid().ToString();
                Client.Subscribe(new string[] { topic }, new byte[] { 2 });
                Client.Connect(_clientId);
                return true;              
            }
            catch (Exception)
            {
                return false;
            }
            //});
        }

        private static void Client_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
                string recivedJson = Encoding.UTF8.GetString(e.Message);

                var nMessage = JsonConvert.DeserializeObject<Message>(recivedJson);
                if (nMessage == null) return;
                Application.Current.Dispatcher.Invoke(delegate
                {
                    Messages.Add(nMessage);        
                });
        }

        public static void Publish() //sprawdzenie czy broker jest wciaz online/ czy wciaz jest polaczenie
        {
            Client.Publish("Ping", Encoding.UTF8.GetBytes("ping"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
        }
}
}

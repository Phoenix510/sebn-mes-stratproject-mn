﻿using System;
using System.Windows.Data;

namespace MqttCleint.ViewModel
{
    class HeaderMaxWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (parameter == null) return 0;
            double x = (double)value;
            double y = double.Parse((string)parameter);
            return x * y;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}

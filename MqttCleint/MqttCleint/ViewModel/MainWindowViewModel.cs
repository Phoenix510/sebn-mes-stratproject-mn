﻿using MqttCleint.Model;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace MqttCleint.ViewModel
{
    public enum StatusName
    {
        Online,
        Offline,
        TryingToReconnect
    }
    public class MainWindowViewModel: INotifyPropertyChanged
    {
        public ObservableCollection<Message> Messages { get; set; }

        private Visibility _visibilityOfReconnect = Visibility.Collapsed;
        public Visibility VisibilityOfReconnect { get { return _visibilityOfReconnect; } set { _visibilityOfReconnect = value; OnPropertyChanged(); } }

        private StatusName _status = StatusName.Online;
        public StatusName Status { get { return _status; } set { _status = value;  OnPropertyChanged(); } }

        private string _topic = "New Messages";
        public string Topic { get { return _topic; } set { _topic = value; OnPropertyChanged(); } }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public MainWindowViewModel()
        {
            FirstConnect();        
            Messages = MqttConnHandler.Messages;
        }

        private ICommand _reconnectCommand;
        public ICommand ReconnectCommand
        {
            get
            {
                if (_reconnectCommand == null)
                    _reconnectCommand = new RelayCommand(
                async (object par) =>
                {
                    Status = StatusName.TryingToReconnect;

                   await Task.Run( async () =>
                   {
                       for (int i = 0; i < 4; i++)
                       {
                           if (await Connect())
                           {
                               Application.Current.Dispatcher.Invoke(delegate
                               {
                                   Status = StatusName.Online;
                                   VisibilityOfReconnect = Visibility.Hidden;
                                   MqttConnHandler.Client.ConnectionClosed += Client_ConnectionClosed;
                               });
                               return;
                           }
                       }                 

                        Application.Current.Dispatcher.Invoke(delegate
                        {
                            Status = StatusName.Offline; 
                            VisibilityOfReconnect = Visibility.Visible;
                        }); 
                    });


                },
                 (object par) =>
                 {
                     if (Status == StatusName.Offline)
                         return true;
                     return false;
                 });


                return _reconnectCommand;
            }
        }
        private async void FirstConnect()
        {
            var result = await MqttConnHandler.SubscribeAndConnect(Topic, "127.0.0.1");
            if (!result) { Status = StatusName.Offline; VisibilityOfReconnect = Visibility.Visible; return; }
            VisibilityOfReconnect = Visibility.Collapsed;
            Status = StatusName.Online;
            MqttConnHandler.Client.ConnectionClosed += Client_ConnectionClosed;
        }

        private void Client_ConnectionClosed(object sender, EventArgs e)
        {
            Status = StatusName.Offline;
            VisibilityOfReconnect = Visibility.Visible;
        }

        private async Task<bool> Connect()
        {
            var result = await MqttConnHandler.SubscribeAndConnect(Topic, "127.0.0.1");
            if (!result)  return false; 

            return true;
        }
    }
}

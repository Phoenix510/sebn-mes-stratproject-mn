﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MessagesAPI.Models;

namespace MessagesAPI.Data
{
    public class MessagesAPIContext : DbContext
    {
        public MessagesAPIContext (DbContextOptions<MessagesAPIContext> options)
            : base(options)
        {
        }

        public DbSet<MessagesAPI.Models.Message> Message { get; set; }
    }
}

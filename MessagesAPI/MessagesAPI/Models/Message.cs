﻿using System;

namespace MessagesAPI.Models
{
    public class Message
    {
        public Guid Id { get; set; }
        public string Nick { get; set; }
        public string Content { get; set; }
        public DateTime DateOfCreation { get; set; }

        public Message() { }
    }
}

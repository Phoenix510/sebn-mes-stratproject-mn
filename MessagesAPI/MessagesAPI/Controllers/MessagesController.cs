﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MessagesAPI.Data;
using MessagesAPI.Models;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using uPLibrary.Networking.M2Mqtt;
using System.Net;

namespace MessagesAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    public class MessagesController : Controller
    {
        private readonly MessagesAPIContext _context;

        public MessagesController(MessagesAPIContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<List<Message>> GetAll([FromBody]DateTime lastDate)
        {

            return await _context.Message.Where(x => x.DateOfCreation > lastDate).OrderBy(x => x.DateOfCreation).Take(200).ToListAsync();
            //return await _context.Message.OrderBy(x=> x.DateOfCreation).Take(200).ToListAsync();
        }

        [HttpPost]
        public async Task<List<Message>> GetMoreMessages([FromBody]DateTime lastDate)
        {
            return await _context.Message.Where(x=> x.DateOfCreation > lastDate).OrderBy(x => x.DateOfCreation).Take(200).ToListAsync();
        }

        public async Task<string> Get()
        {
            return "test2";
        }
        public async Task<List<Message>> GetLast10()
        {
            return await _context.Message.OrderByDescending(x => x.DateOfCreation).Take(10).ToListAsync();
        }

        [HttpPost]
        public async Task<bool> Post([FromBody]Message message)
        {
            if (ModelState.IsValid)
            {
                message.Id = Guid.NewGuid();
                _context.Add(message);
                await _context.SaveChangesAsync();

                try
                {
                    MqttClient client = new MqttClient(IPAddress.Parse("172.20.128.2")); 

                    string clientId = Guid.NewGuid().ToString();
                    client.Connect(clientId);

                    string strValue = Convert.ToString(JsonConvert.SerializeObject(message));
                    client.Publish("New Messages", Encoding.UTF8.GetBytes(strValue), uPLibrary.Networking.M2Mqtt.Messages.MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                    //client.Disconnect();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
                return true;
            }
            return false;
        }
    }
}

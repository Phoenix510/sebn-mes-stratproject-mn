﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BlazorAppMess.Models
{
    public class WebHandler
    {
        public static readonly HttpClient client = new HttpClient();
        public static readonly string error = "Can't reach the server. Try again later!";
        private const string serverUri = "http://localhost:8080/";

        public static async Task<string> PostAsync(string uri, string toSend)
        {
            StringContent data = new StringContent(toSend, Encoding.UTF8, "application/json");

            try
            {
                HttpResponseMessage response = await client.PostAsync(serverUri + uri, data); 
                return response.Content.ReadAsStringAsync().Result;
            }

            catch (Exception)
            {
                return error;
            }
        }

        public static async Task<string> PutAsync(string uri, string toSend)
        {
            StringContent data = new StringContent(toSend, Encoding.UTF8, "application/json");

            try
            {
                HttpResponseMessage response = await client.PutAsync(serverUri + uri, data); 
                return response.Content.ReadAsStringAsync().Result;
            }

            catch (Exception) 
            {
                return error;
            }
        }

        public static async Task<string> GetAsync(string uri)
        {
            try
            {
                string url = serverUri + uri;
                HttpResponseMessage response = await client.GetAsync(url); 
                return response.Content.ReadAsStringAsync().Result;
            }

            catch (Exception ) 
            {
                return error;
            }
        }

        public static async Task<string> DeleteAsync(string uri)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync(serverUri + uri); 
                return response.Content.ReadAsStringAsync().Result;
            }

            catch (Exception)
            {
                return error;
            }
        }
    }
}


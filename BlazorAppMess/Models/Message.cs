﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorAppMess.Models
{
    public enum Status
    {
        Online,
        Offline
    }
    public class Message
    {
        public Guid Id { get; set; }

        [JsonIgnore]
        private string _nick;
        public string Nick { get { return _nick ?? "Anonymous"; } set { _nick = value; } }
        public string Content { get; set; }
        public DateTime DateOfCreation { get; set; }

        [JsonIgnore]
        public DateTime LocalDateOfCreation => (DateTime.SpecifyKind(DateOfCreation, DateTimeKind.Utc)).ToLocalTime();

        public Message(string content, DateTime createDate, string nick = null)
        {
            Id = Guid.NewGuid();
            Content = content;
            DateOfCreation = createDate;
            _nick = nick;
        }
    }
}

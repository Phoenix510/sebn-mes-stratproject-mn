#pragma checksum "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9694c7269ba9400fa6928a0e3e839577569bb0e1"
// <auto-generated/>
#pragma warning disable 1591
namespace BlazorAppMess.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\_Imports.razor"
using BlazorAppMess;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\_Imports.razor"
using BlazorAppMess.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\_Imports.razor"
using Blazored.LocalStorage;

#line default
#line hidden
#nullable disable
#nullable restore
#line 90 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
using BlazorAppMess.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 92 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
using Newtonsoft.Json;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/")]
    public partial class Index : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, @"<style>
    #nickInput {
        min-width: 270px;
        width: 20vw;
        text-align: left;
        padding-left: 20px;
    }

    #messageInput {
        padding: 5px;
        width: 25vw;
        min-width: 300px;
        margin-right: 10px;
        border-radius: 5px;
        text-align: left;
        float: left;
        padding-left: 20px;
        margin-bottom: 2vh;
    }

    #sendButton {
        padding: 5px;
        width: 8vw;
        min-width: 80px;
        border-radius: 5px;
    }

    td {
        max-width: 30vw;
        overflow-wrap: break-word;
    }

    #info {
        text-align: right;
    }
</style>

");
            __builder.OpenElement(1, "div");
            __builder.AddAttribute(2, "class", "row");
            __builder.AddMarkupContent(3, "<div class=\"col-8\"><h1>Hello, customer!</h1>\r\n        <p style=\"margin-bottom:2vh;\">Welcome in a message sender application.</p></div>\r\n    ");
            __builder.OpenElement(4, "div");
            __builder.AddAttribute(5, "class", "col-4");
            __builder.OpenElement(6, "p");
            __builder.AddAttribute(7, "id", "info");
            __builder.AddContent(8, "Status: ");
            __builder.AddContent(9, 
#nullable restore
#line 46 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
                              _status

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(10, "\r\n\r\n");
            __builder.OpenElement(11, "input");
            __builder.AddAttribute(12, "class", "input-group-text");
            __builder.AddAttribute(13, "id", "nickInput");
            __builder.AddAttribute(14, "placeholder", "Insert your nick here (optional)");
            __builder.AddAttribute(15, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 51 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
                                                                                                     _nick

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(16, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => _nick = __value, _nick));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(17, "\r\n<div style=\"clear: both; margin-bottom: 2vh;\"></div>\r\n\r\n");
            __builder.OpenElement(18, "input");
            __builder.AddAttribute(19, "class", "input-group-text");
            __builder.AddAttribute(20, "id", "messageInput");
            __builder.AddAttribute(21, "type", "text");
            __builder.AddAttribute(22, "placeholder", "Insert your message");
            __builder.AddAttribute(23, "onkeyup", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.KeyboardEventArgs>(this, 
#nullable restore
#line 54 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
                                                                                                                             SendMessageOnEnter

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(24, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 54 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
                                                                                                       _nMessage

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(25, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => _nMessage = __value, _nMessage));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(26, "\r\n");
            __builder.OpenElement(27, "button");
            __builder.AddAttribute(28, "class", "btn-primary");
            __builder.AddAttribute(29, "id", "sendButton");
            __builder.AddAttribute(30, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 55 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
                                                      SendMessageOnClick

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(31, "Send");
            __builder.CloseElement();
            __builder.AddMarkupContent(32, "\r\n\r\n");
            __builder.AddMarkupContent(33, "<h2 style=\"text-align:left; margin-top: 2vh;\">Last 10 messages:</h2>");
#nullable restore
#line 58 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
 if (_messages == null)
{

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(34, "<p><em>Loading...</em></p>");
#nullable restore
#line 61 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
}
else if (_failed)
{

#line default
#line hidden
#nullable disable
            __builder.OpenElement(35, "p");
            __builder.OpenElement(36, "em");
            __builder.OpenElement(37, "strong");
            __builder.AddContent(38, 
#nullable restore
#line 64 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
                    WebHandler.error

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 65 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
}
else
{

#line default
#line hidden
#nullable disable
            __builder.OpenElement(39, "table");
            __builder.AddAttribute(40, "class", "table");
            __builder.AddMarkupContent(41, "<thead><tr><th>Date</th>\r\n                <th>Sender</th>\r\n                <th>Content</th></tr></thead>\r\n        ");
            __builder.OpenElement(42, "tbody");
#nullable restore
#line 77 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
             foreach (var message in _messages)
            {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(43, "tr");
            __builder.OpenElement(44, "td");
            __builder.AddContent(45, 
#nullable restore
#line 80 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
                         message.LocalDateOfCreation.ToString("dd/MM/yyyy HH:mm")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(46, "\r\n                    ");
            __builder.OpenElement(47, "td");
            __builder.AddContent(48, 
#nullable restore
#line 81 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
                         message.Nick

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(49, "\r\n                    ");
            __builder.OpenElement(50, "td");
            __builder.AddContent(51, 
#nullable restore
#line 82 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
                         message.Content

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 84 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
            }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 87 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
#nullable restore
#line 94 "C:\Users\marek\source\repos\SEBN\BlazorAppMess\Pages\Index.razor"

    private List<Message> _messages = null;
    private string _nMessage;
    private string _nick = null;
    private bool _failed = false;
    private Status _status = Status.Offline;
    //private DateTime? _dateOfUpdate = null;
    //private string _updated => _dateOfUpdate == null ? "Never" : DateTime.Parse(_dateOfUpdate.ToString()).ToString("dd/MM/yyyy HH:mm");

    private void SendMessageOnClick()
    {
        SendMessage();
    }

    private void SendMessageOnEnter(KeyboardEventArgs e)
    {
        if (e.Code != "Enter" && e.Code != "NumpadEnter") return;

        SendMessage();
    }

    protected override async Task OnInitializedAsync()
    {
        var data = await WebHandler.GetAsync("api/messages/getLast10");
        if (data == WebHandler.error) { _messages = new List<Message>(); _failed = true; _status = Status.Offline; return; }

        _messages = JsonConvert.DeserializeObject<List<Message>>(data);
        _status = Status.Online;
        //_dateOfUpdate = DateTime.Now;

        if (_messages.Count < 1) { _messages = new List<Message>(); return; }
    }

    public async void SendMessage()
    {
        if (string.IsNullOrWhiteSpace(_nMessage)) { await JsRuntime.InvokeAsync<object>("alert", "Message cannot be empty"); return; }
        if (_nMessage.Length > 400) { await JsRuntime.InvokeAsync<object>("alert", $"Message can't have more than 400 chars. \nYour message has {_nMessage.Length} chars!"); return; }
        if (_nick?.Length > 15) { await JsRuntime.InvokeAsync<object>("alert", $"Your nick cannot be larger than 15 chars. \nYour nick has {_nick.Length} chars!"); return; }

        if (string.IsNullOrWhiteSpace(_nick)) _nick = null;
        var newMessage = new Message(_nMessage, DateTime.UtcNow, _nick);
        _messages.Insert(0, newMessage);
        if (_messages.Count > 9)
        {
            _messages.Remove(_messages.Last());
        }
        _nMessage = string.Empty;

        string serializedMessage = JsonConvert.SerializeObject(newMessage);
        var response = await WebHandler.PostAsync("api/messages/Post", serializedMessage);
        if (response == WebHandler.error) { await JsRuntime.InvokeAsync<object>("alert", WebHandler.error); _status = Status.Offline; return; }
        _status = Status.Online;
    }



#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Blazored.LocalStorage.ILocalStorageService localStore { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JsRuntime { get; set; }
    }
}
#pragma warning restore 1591
